import array_helper as helper
from warnings import warn

class Analyser:

    scale = ['c', ['db','c#'], 'd', ['eb','d#'], 'e', 'f', ['gb','f#'], 'g', ['ab','g#'], 'a', ['a#','bb'], 'b']

    def __init__(self, pitches, is_verbose=False):
        self.pitches = pitches
        self.pc_set = []
        self.normal_form = []
        self.prime_form = []
        self.is_verbose = is_verbose

    def extract_pcs(self, pitches):
        classes = set()
        for pitch in pitches:
            pitch = pitch.replace(" ", "")
            for i, p in enumerate(Analyser.scale):
                if isinstance(p, list) and pitch in p:
                    classes.add(i)
                    break
                elif p == pitch:
                    classes.add(i) 
                    break
        return list(classes)

    def transpose(self, pc_set):
        base = 12 - pc_set[0]
        transposed = [ (item + base) % 12 for item in pc_set ]
        if self.is_verbose:
            print('computing transposition')
            print(pc_set, '->', transposed)
        return transposed

    def invert(self, pc_set, is_verbose=False):
        inverted = [ (12 - item) % 12 for item in pc_set ]
        if self.is_verbose:
            print('computing inversion')
            print(pc_set, '->', inverted)
        return inverted
    
    def find_most_compact(self, pc_sets, n=0):
        weights = []
        for pc_set in pc_sets:
            weight = pc_set[len(pc_set) - 1 - n] - pc_set[0]
            weights.append((12 + weight) % 12)

        norms = [pc_sets[i] for i, weight in enumerate(weights) if weight == min(weights)]

        if self.is_verbose:
            print('looking for most compact')
            print(norms)

        if len(norms) == 1 or (len(norms) == 2 and norms[0] == norms[1]):
            return norms[0]
        elif len(norms) > 1 and n < len(pc_sets[0]):
            if self.is_verbose:
                print('there is more to compare. Recursing')
            return self.find_most_compact(norms, n=n+1)
        else:
            warn(f"! could not find most compact form from {pc_sets}. Exiting function")
            return -1
            
    def compute_rotations(self, pc_set):
        rotations = []
        last_rotation = pc_set
        last_rotation.sort()
        for i in range(len(pc_set)):
            rotations.append(last_rotation)
            last_rotation = helper.rotate_list(last_rotation)
        return rotations
    
    def compute_normal_form(self, pc_set):
        if self.is_verbose:
            print('\ncomputing normal form:\n')
        rotations = self.compute_rotations(pc_set)
        return self.find_most_compact(rotations)

    def compute_prime_form(self, pc_set):
        norm_transposed = self.transpose(pc_set)
        inverted = self.invert(norm_transposed)
        norm_inverted = self.compute_normal_form(inverted)
        transpose_inverted = self.transpose(norm_inverted)
        prime_form = self.find_most_compact([norm_transposed, transpose_inverted])
        if self.is_verbose:
            print('\ncomputing prime form:\n')
            print('transposed normal form:', norm_transposed)
            print('inversion:', norm_inverted)
            print('transposed inversion:', transpose_inverted)
            print('finding most compact', norm_transposed, '<->', transpose_inverted)
            print('prime form:', f"({''.join(map(str, prime_form))})")
        return prime_form

    @staticmethod
    def analyse(pitches, is_verbose=False):
        obj = Analyser(pitches, is_verbose)
        obj.pc_set = obj.extract_pcs(pitches)
        obj.normal_form = obj.compute_normal_form(obj.pc_set)
        obj.prime_form = obj.compute_prime_form(obj.normal_form)
        return obj
    
    def __str__(self):
        if self.prime_form == -1:
            return f"No prime form could be found for {self.pitches}!"
        return f"Pitches: {self.pitches}\nPitch class set: {self.pc_set}\nNormal Form: {self.normal_form}\nPrime form: ({''.join(map(str, self.prime_form))})"