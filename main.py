#!/usr/bin/env python3

import argparse
import csv
from analyser_cls import Analyser


def init():

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', nargs='?', default='./data/data.csv', help="Pass path to csv data file")
    parser.add_argument('-v', '--verbose', action='store_true', help="Set verbose flag")
    parser.add_argument('-p', '--pitches', nargs='*', help="space separated pitches")
    args = parser.parse_args()

    results = []

    if not args.pitches :
        with open(args.data) as csv_file:
            for row in csv.reader(csv_file):
                analysis = Analyser.analyse(row, is_verbose=args.verbose)
                results.append(analysis)
    else:
        analysis = Analyser.analyse(args.pitches, is_verbose=args.verbose)
        results.append(analysis)

    print('\nresults:')
    for analyse in results:
        print('-------------------')
        print(analyse)

    

if __name__ == "__main__":
    init()