def rotate_list(arr, direction='right'):
    if direction == 'right':
        return arr[-1:] + arr[:-1]
    elif direction == 'left':
        return arr[1:] + arr[:1]
    else:
        raise ValueError(f"Wrong list rotation direction: {direction} unknown")