# Pitch Class Set Analyser

#### Nothing serious here.  

Simply a small quick&dirty program that analyses a sequence of notes according to the Set Theory.  
Supports enharmonic equivalence (fancy right?)  

Make main.py be an excutable  

```
cd path/to/pitch_class_set
chmod +x main.py
```

Usage:  

Execute with default data.csv file from the /data folder
```
./main.py
```

Pass the notes sequence as a command line argument
```
./main.py -p c d e g#
```

Set verbose flag (prints all the relevant steps executed by the program)
```
./main.py -p c d e g# -v
```

Get help
```
./main.py -h
```